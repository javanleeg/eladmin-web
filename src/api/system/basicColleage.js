import request from '@/utils/request'

export function getColleage(params) {
  return request({
    url: 'api/basicColleage',
    method: 'get',
    params
  })
}

export function getColleageSuperior(ids) {
  const data = ids.length || ids.length === 0 ? ids : Array.of(ids)
  return request({
    url: 'api/basicColleage/superior',
    method: 'post',
    data
  })
}

export function add(data) {
  return request({
    url: 'api/basicColleage',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/basicColleage/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/basicColleage',
    method: 'put',
    data
  })
}

export default { add, edit, del }
