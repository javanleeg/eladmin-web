const demoList = {
  status: 200,
  message: 'success',
  data: {
    total: 40,
    'rows|40': [{
      student_id: '@guid',
      colleage_id: '@guid',
      major_id: '@guid',
      student_code: '@guid',
      student_name: '@cname',
      'gender|1': ['男', '女'],
      phone: '@phone',
      username: '@cname',
      'enabled|1': [1, 2],
      'update_time': '@DATETIME("yyyy-MM-dd HH:mm:ss")'

    }]
  }
}
export default {
  'get|/parameter/query': demoList
}
